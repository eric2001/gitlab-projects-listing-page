#!/bin/bash
#	GitLab Projects Listing Page -- Template project for building a GitLab page that displays a list of projects for the user.
#	Copyright 2021 - 2022 Eric Cavaliere
#	Project Start Date:  09 April 2021
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Page Settings -- Edit this section to customize your site.

#General Settings
PageTitle="Example Page"
PageTheme="dark"
RedirectURL="http:\/\/MYUSERNAME.gitlab.io\/SignIn.html"
ApplicationId=""
FooterText="See the <a href=\"https:\/\/gitlab.com\/eric2001\/gitlab-projects-listing-page\" target=\"_blank\">project page<\/a> for more information."
ApiSiteRoot="https:\/\/gitlab.com\/api"
OAuthUrl="https:\/\/gitlab.com\/oauth\/token"
ApiVersion="v4"
UsersToLoad=""
ProjectsToSkip=""
CreatorId="null"

# Card Settings
CardTagDefaultCss="bg-light text-dark"
CardTagSpecificCss=""
TagCategorySortOrder=""

# Search Settings
SearchDefaultTagCss="btn-outline-light"
SearchTagSpecificCss=""
SearchCategoriesOrder=""
SearchCategoriesHidden=""

# End of Page Settings

# Functions
# Source: https://stackoverflow.com/questions/296536/how-to-urlencode-data-for-curl-command
rawurlencode() {
  local string="${1}"
  local strlen=${#string}
  local encoded=""
  local pos c o

  for (( pos=0 ; pos<strlen ; pos++ )); do
     c=${string:$pos:1}
     case "$c" in
        [-_.~a-zA-Z0-9] ) o="${c}" ;;
        * )               printf -v o '%%%02x' "'$c"
     esac
     encoded+="${o}"
  done
  echo "${encoded}"    # You can either set a return variable (FASTER) 
  REPLY="${encoded}"   #+or echo the result (EASIER)... or both... :p
}
# End Functions

# Begin Deployment

# Copy all source files over
echo 'copying files to destination'
mv source/ public/

# Files to Update
indexfile="public/index.html"
signinfile="public/SignIn.html"
jsfile="public/js/gitlab.js"

# Update the index.html file
echo 'Begin index.html'
rawurlencode $RedirectURL
echo 'TITLE'
search="{TITLE}"
sed -i "s/$search/$PageTitle/" $indexfile
echo 'THEME'
search="{THEME}"
sed -i "s/$search/$PageTheme/" $indexfile
echo 'REDIRECTURL'
search="{REDIRECTURL}"
sed -i "s/$search/$REPLY/" $indexfile
echo 'APPLICATIONID'
search="{APPLICATIONID}"
sed -i "s/$search/$ApplicationId/" $indexfile
echo 'FOOTERTEXT'
search="{FOOTERTEXT}"
sed -i "s/$search/$FooterText/" $indexfile
echo 'index.html Complete'

# Update the SignIn.html file
echo 'Begin SignIn.html'
echo 'TITLE'
search="{TITLE}"
sed -i "s/$search/$PageTitle/" $signinfile
echo 'THEME'
search="{THEME}"
sed -i "s/$search/$PageTheme/" $signinfile
echo 'REDIRECTURL'
search="{REDIRECTURL}"
sed -i "s;$search;$RedirectURL;" $signinfile
echo 'APPLICATIONID'
search="{APPLICATIONID}"
sed -i "s/$search/$ApplicationId/" $signinfile
echo 'FOOTERTEXT'
search="{FOOTERTEXT}"
sed -i "s/$search/$FooterText/" $signinfile
echo 'OAUTHURL'
search="{OAUTHURL}"
sed -i "s/$search/$OAuthUrl/" $signinfile
echo 'SignIn.html Complete'

# Update the gitlab.js file
echo 'Begin gitlab.js'
echo 'TITLE'
search="{TITLE}"
sed -i "s/$search/$PageTitle/" $jsfile
echo 'APISITEROOT'
search="{APISITEROOT}"
sed -i "s/$search/$ApiSiteRoot/" $jsfile
echo 'APIVERSION'
search="{APIVERSION}"
sed -i "s/$search/$ApiVersion/" $jsfile
echo 'USERSTOLOAD'
search="{USERSTOLOAD}"
sed -i "s/$search/$UsersToLoad/" $jsfile
echo 'PROJECTSTOSKIP'
search="{PROJECTSTOSKIP}"
sed -i "s/$search/$ProjectsToSkip/" $jsfile
echo 'CREATORID'
search="{CREATORID}"
sed -i "s/$search/$CreatorId/" $jsfile

echo 'CARDTAGDEFAULTCSS'
search="{CARDTAGDEFAULTCSS}"
sed -i "s/$search/$CardTagDefaultCss/" $jsfile
echo 'CARDTAGSPECIFICCSS'
search="{CARDTAGSPECIFICCSS}"
sed -i "s/$search/$CardTagSpecificCss/" $jsfile
echo 'TAGCATEGORYSORTORDER'
search="{TAGCATEGORYSORTORDER}"
sed -i "s/$search/$TagCategorySortOrder/" $jsfile

echo 'SEARCHDEFAULTTAGCSS'
search="{SEARCHDEFAULTTAGCSS}"
sed -i "s/$search/$SearchDefaultTagCss/" $jsfile
echo 'SEARCHTAGSPECIFICCSS'
search="{SEARCHTAGSPECIFICCSS}"
sed -i "s/$search/$SearchTagSpecificCss/" $jsfile
echo 'SEARCHCATEGORIESORDER'
search="{SEARCHCATEGORIESORDER}"
sed -i "s/$search/$SearchCategoriesOrder/" $jsfile
echo 'SEARCHCATEGORIESHIDDEN'
search="{SEARCHCATEGORIESHIDDEN}"
sed -i "s/$search/$SearchCategoriesHidden/" $jsfile

echo 'gitlab.js Complete'

# End of Deployment

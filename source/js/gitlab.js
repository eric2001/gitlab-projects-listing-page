/*
	GitLab Projects Listing Page -- Template project for building a GitLab page that displays a list of projects for the user.
	Copyright 2021 - 2022 Eric Cavaliere
	Project Start Date:  09 April 2021

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

var tags = {};
var ajaxCalls = 0;
var TagIndexes = new Array();
var SelectedTags = new Array();
var LoadedProjectIds = '';

// General Configuration
var SiteName = "{TITLE}";
var apiSiteRoot = '{APISITEROOT}';
var apiVersion = '{APIVERSION}';
var UsersToLoad = { {USERSTOLOAD} } // List of users and groups to load projects.  Indicate if each one belongs to a user or a group to control which requests get made, ex: "users": 'myuser' or "groups": 'mygroup'.
var ProjectsToSkip = [ {PROJECTSTOSKIP} ]; // Comma separated list of project path_with_namespace values that should never be displayed.  Leave blank to load everything.
var CreatorId = {CREATORID}; // Only display projects created by this user.  If null projects from all users will be displayed.

// Card configuration
var CardTagDefaultCss = '{CARDTAGDEFAULTCSS}';
var CardTagSpecificCss = { {CARDTAGSPECIFICCSS} };
var TagCategorySortOrder = [ {TAGCATEGORYSORTORDER} ];

// Search screen configuration
var SearchDefaultTagCss = '{SEARCHDEFAULTTAGCSS}';
var SearchTagSpecificCss = { {SEARCHTAGSPECIFICCSS} };
var SearchCategoriesOrder = [ {SEARCHCATEGORIESORDER} ];
var SearchCategoriesHidden = [ {SEARCHCATEGORIESHIDDEN} ];

// Handles initial page load
function navigate(pageToLoad) {
  if (pageToLoad == "#Projects") {
    showScreen('repositories');
  } else if (pageToLoad.startsWith("#Tags")) {
    showScreen('repositories');
    $('#Tags').collapse('show');

    // If any tag names were passed in through the URL, filter for those tags.
    if (~pageToLoad.indexOf(":")) {
      // unselect any active selections
      $('.btn-check').prop( "checked", false );
	  SelectedTags = new Array();

      // Figure out what was specified in the URL string and select it.
      var TagsFromUrl = decodeURIComponent(pageToLoad.substring(pageToLoad.indexOf(':')+1));
      $.each(TagsFromUrl.split(','), function( key, value ) {
        var CurrentCategory = '';
        var CurrentTag = '';
        if (~value.indexOf(":")) {
          var SplitTag = value.split(":");
          CurrentCategory = SplitTag[0].trim();
          CurrentTag = SplitTag[1].trim();
        } else {
          CurrentCategory = "[GENERAL]"
          CurrentTag = value.trim();
        }
        $('#btncheck' + GetTagId(CurrentTag, CurrentCategory)).click();
      });
    }
  } else if (pageToLoad.startsWith("#Documentation:")) {
    DisplayDocumentation(pageToLoad.replace('#Documentation:', ''));
    showScreen('documentation');
  } else if (pageToLoad == "#History") {
    showScreen('history');
  } else if (pageToLoad == "#SignOut") {
    SignOut();
    return false;
  }
}

// Get the session token, if it exists, otherwise return an empty string.
function SignIn() {
  var accessToken = '';
  var tokenitem = document.cookie.split(';').find(function(i){return $.trim(i).split('=')[0]=='access_token'})
  if (tokenitem) { 
    accessToken = tokenitem.split('=')[1];
  }
  
  return accessToken;
}

// Handles signing out of gitlab oauth
function SignOut() {
  document.cookie = 'access_token=;path=/';
  window.location.hash = "Projects";
  window.location.reload(true);
}

// Handles changing screens for main navigation
function showScreen(screen_name) {
  $('main').attr("hidden", true); // Hide everything.
  if ($('#nav-link-tags').attr('aria-expanded') == "true") {
	  $('#Tags').collapse('hide'); // Hide list of tags if currently displayed.
  }
  $('a[id^="nav-link-"], a.text-secondary').removeClass('text-secondary').addClass('header-link'); // Unselect all buttons

  $('#container-' + screen_name).attr("hidden", false); // Show the requested screen.
  $('a[id="nav-link-' + screen_name + '"]').removeClass('header-link').addClass('text-secondary'); // Select the button that was clicked.
  if (screen_name == 'history') {
    $(document).prop('title', SiteName + ': History');
  } else if (screen_name == 'documentation') {
    // Do nothing, show documentation handles everything.
  } else {
    $(document).prop('title', SiteName + ': Projects');
  }
  return true;
}

// Build the HTML for one repository card.
function BuildRepositoryCard(repositoryObject) {
  // Replace null values that are displayed on the card with some default values.
  if (repositoryObject.avatar_url == null) {
    repositoryObject.avatar_url = 'images/project.png';
  }
  if (repositoryObject.description == null) {
    repositoryObject.description = '';
  }
  if (repositoryObject.readme_url == null) {
    repositoryObject.readme_url = '';
  }

  // If this projects name and namespace are on the skip list, then skip it.
  if (ProjectsToSkip.length > 0 && $.inArray(repositoryObject.path_with_namespace, ProjectsToSkip ) >=0 ) {
    return '';
  }

  // If CreatorId is set, then only display projects for the supplied id or where the project doesn't have a creator.
  if (CreatorId != null && (repositoryObject.creator_id !== undefined && repositoryObject.creator_id != CreatorId)) {
    return '';
  }

  // Store off the ID to make it easier to figure out what projects were displayed later.
  LoadedProjectIds += '|' + repositoryObject.id + '|';

  // Generate the html for the card.
  var CardTagIds = '';
  var html = '';
  var cardTitle = jQuery('<div />').text(repositoryObject.name).html();
  var cardDescription = jQuery('<div />').text(repositoryObject.description).html();
  html += '<div class="col repository-card" id="repository-card-' + repositoryObject.id + '">';
  html += '<div class="card text-left repository-card-content">';
  html += '<div class="card-header">';
  html += '<h5 class="card-header-style card-title">' + cardTitle + '</h5>';
  html += '</div>';
  html += '<div class="card-body">';
  html += '<div class="row g-2">';
  html += '<div class="card-logo-container col">';
  html += '<img class="card-logo" src="' + repositoryObject.avatar_url + '" alt="' + cardTitle + '" />';
  html += '</div>';
  html += '<div class="col">';
  if (repositoryObject.readme_url == '') {
    html += '<a href="' + repositoryObject.web_url + '" class="stretched-link" id="repository-card-link-' + repositoryObject.id + '" target="_blank"></a>';
  } else {
    html += '<a href="#Documentation:' + repositoryObject.id + '" onclick="return navigate(\'#Documentation:' + repositoryObject.id + '\')" class="stretched-link" id="repository-card-link-' + repositoryObject.id + '"></a>';
  }
  html += '<p class="card-text card-description">' + cardDescription + '</p>';
  html += '<input type="hidden" class="card-namespace" id="cardNamespace' + repositoryObject.id + '" value="' + repositoryObject.path_with_namespace + '" />';
  html += '<input type="hidden" class="card-readme" id="cardReadme' + repositoryObject.id + '" value="' + repositoryObject.readme_url + '" />';
  html += '<input type="hidden" class="card-default-branch" id="cardDefaultBranch' + repositoryObject.id + '" value="' + repositoryObject.default_branch + '" />';
  html += '<input type="hidden" class="card-repository-url" id="cardRepositoryUrl' + repositoryObject.id + '" value="' + repositoryObject.web_url + '" />';
  html += '</div>';
  html += '</div>';
  html += '</div>';

  html += '<div class="card-footer" id="cardFooter' + repositoryObject.id + '">';
  html += '<div class="card-footer-text overflow-hidden">';

  // Sort the list of tags on the bottom of the cards.
  //   Sort order is tags without categories (alphabetical), followed by tags with categories (alphabetical by category, then by tag).
  //   TagCategorySortOrder can be used to move specific categories to the beginning of the list, overriding the default sort order.
  repositoryObject.tag_list.sort(function(a,b){
    var compA = TagCategorySortOrder.length;
    var compB = TagCategorySortOrder.length;
	var categoryA = '';
	var categoryB = '';
    var tagA = a;
    var tagB = b;
	
    if (~a.indexOf(":")) {
      var SplitTag = a.split(":");
      categoryA = SplitTag[0].trim();
      var tagA = SplitTag[1].trim();
      // If the tag contains a ":" and if the category is in the sorted list, then use it's location in the array to sort it.
      compA = jQuery.inArray(categoryA, TagCategorySortOrder);
      // If the tag's category isn't in the list, display it at the end of the list.
	  if (compA == -1) {
        compA = TagCategorySortOrder.length + 2;
	  }
    } else {
        // If the tag doesn't have a category, display it first, or right after the categories that were prioritized to the beginning of the list.
        compA = TagCategorySortOrder.length + 1;
    }
    if (~b.indexOf(":")) {
      var SplitTag = b.split(":");
      var categoryB = SplitTag[0].trim();
      var tagB = SplitTag[1].trim();
      // If the tag contains a ":" and if the category is in the sorted list, then use it's location in the array to sort it.
      compB = jQuery.inArray(categoryB, TagCategorySortOrder);
      // If the tag's category isn't in the list, display it at the end of the list.
	  if (compB == -1) {
        compB = TagCategorySortOrder.length + 2;
      }
    } else {
        // If the tag doesn't have a category, display it first, or right after the categories that were prioritized to the beginning of the list.
        compB = TagCategorySortOrder.length  + 1;
    }

    // If position is equal and category is equal, sort by tag (alphabetical).
    // If position is equal and category is not equal, sort by category (alphabetical).
    // Otherwise sort by position.
    if (compA == compB) {
      if (categoryA == categoryB) {
        return (tagA.toLowerCase() < tagB.toLowerCase()) ? -1 : (tagA.toLowerCase() > tagB.toLowerCase()) ? 1 : 0;
      } else {
        return (categoryA.toLowerCase() < categoryB.toLowerCase()) ? -1 : (categoryA.toLowerCase() > categoryB.toLowerCase()) ? 1 : 0;
      }
    } else {
      return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
    }
  });

  // Generate html to display the list of tags.
  $.each(repositoryObject.tag_list, function( key, value ) {
    var tagText = jQuery('<div />').text(value).html();
    html += BuildRepositoryTag(tagText);

    if (~tagText.indexOf(":")) {
      var SplitTag = tagText.split(":");
      var Category = SplitTag[0].trim();
      var SubTagName = SplitTag[1].trim();
      CardTagIds += '|' + GetTagId(SubTagName, Category);
    } else {
      CardTagIds += '|' + GetTagId(tagText, "[GENERAL]");
    }
  });
  CardTagIds += '|';
  html += '</div>';
  html += '</div>';
  html += '</div>';
  html += '<input type="hidden" class="card-assigned-tags" id="cardTags' + repositoryObject.id + '" value="' + CardTagIds + '" />';
  html += '</div>';

  return html;
}

// Build HTML to display a tag on a repository card.
function BuildRepositoryTag(tagObject) {
  UpdateTagsList(tagObject);

  // Figure out the category.
  var category = '[GENERAL]';
  if (~tagObject.indexOf(":")) {
    var SplitTag = tagObject.split(":");
    category = SplitTag[0].trim();
  }

  // Use the category to figure out what CSS to use.
  var cssStyle = CardTagDefaultCss;
  $.each( CardTagSpecificCss, function( key, value ) {
    if (key == category) {
      cssStyle = value;
    }
  });

  // Use the category to figure out if this tag should be clickable or not.
  var disableLink = jQuery.inArray(category, SearchCategoriesHidden);

  // Build the html
  var html = '';
  html += '<span class="badge ';
  html += cssStyle;
  if (disableLink >= 0) {
    html += '">';
  } else {
    html += '" onclick="return navigate(\'#Tags:' + tagObject + '\')" >'
  }  
  html += tagObject;
  html += '</span> ';

  return html;
}

// Maintain a list of all tags used by repository cards as the cards html is generated.
function UpdateTagsList(TagName) {
  if (~TagName.indexOf(":")) {
    var SplitTag = TagName.split(":");
    var Category = SplitTag[0].trim();
    var SubTagName = SplitTag[1].trim();
    if (tags[Category] == null) {
      tags[Category] = new Array();
    }
    if (jQuery.inArray(Category, TagIndexes) == -1) {
      TagIndexes[TagIndexes.length] = Category;
    }
    if (jQuery.inArray(SubTagName, tags[Category]) == -1) {
      tags[Category][tags[Category].length] = SubTagName;
    }
  } else {
    if (tags["[GENERAL]"] == null) {
      tags["[GENERAL]"] = new Array();
    }
    if (jQuery.inArray(TagName.trim(), tags["[GENERAL]"]) == -1) {
      tags["[GENERAL]"][tags["[GENERAL]"].length] = TagName.trim();
    }
  }
}

// Build the filter options under the tags menu.
//  Display order is tags without categories  (alphabetical), then each category (alphabetical).
//  SearchCategoriesOrder can be used to bring specific categories to the beginning of the categories section, overriding the alphabetical sort.
//  SearchCategoriesHidden can be used to exclude specific categories from the search screen.
function BuildTagsScreen() {
  var html = '';
  var IndexesOrder = new Array();   // Final display order for all categories.

  // Make tags without categories the first item to display. 
  IndexesOrder[IndexesOrder.length] = '[GENERAL]';

  // Display any categories in SearchCategoriesOrder right after tags without categories.
  $.each(SearchCategoriesOrder, function( key, value ) {
    IndexesOrder[IndexesOrder.length] = value;
  });
  
  // Build a sorted list of any other categories.
  var TempIndexes = new Array();
  $.each(tags, function( key, value ) {
    // If key (tag without category) is not in the final list, the ignore list of the temporary list already, then add it to the temporary list.
    if (jQuery.inArray(key, IndexesOrder) == -1) {
      if (jQuery.inArray(key, SearchCategoriesHidden) == -1) {
        if (jQuery.inArray(key, TempIndexes) == -1) {
          TempIndexes[TempIndexes.length] = key;
        }
      }
    }
  });
  TempIndexes.sort();
  
  // Merge the sorted list of any other categories into the final list.
  $.each(TempIndexes, function( key, value ) {
    IndexesOrder[IndexesOrder.length] = value;
  });

  // Build the HTML for each category.
  $.each(IndexesOrder, function( key, value ) {
    html+= BuildFilterTags(value);
  });

  return html;
}

// Generate HTML for one category on the tags screen.
function BuildFilterTags(category) {
  var html = '';

  // Make sure the category is valid before trying to render it.
  if (tags[category] != null && tags[category].length > 0) {

    // Figure out what color to use for this category.
    var cssStyle = SearchDefaultTagCss;
    $.each( SearchTagSpecificCss, function( key, value ) {
      if (key == category) {
        cssStyle = value;
      }
    });

    // General doesn't have a heading, everything else does.
    html += '<div class="row py-1">';
    if (category != "[GENERAL]") {
      html += '<div class="col tag-cell-text tag-cell-header-text">';
      html += '<p><strong>' + category + ':</strong></p>';
      html += '</div>';
    }

    // Sort all the tags in this category before displaying them.
    var sortedList = tags[category].slice().sort(function(a,b){
      return (a.toLowerCase() < b.toLowerCase()) ? -1 : (a.toLowerCase() > b.toLowerCase()) ? 1 : 0;
    });

    // Loop through and display each tag.
    html += '<div class="col tag-cell-text tag-cell-content-text">';
      $.each(sortedList, function( key, value ) {
        html += '<input type="checkbox" class="btn-check" id="btncheck' + GetTagId(value, category) + '" autocomplete="off" onclick="return UpdateRepositoryFilter(\'btncheck' + GetTagId(value, category) + '\')">';
        html += '<label class="btn btn-sm ' + cssStyle + '" for="btncheck' + GetTagId(value, category) + '">' + value + '</label> ';
      });
    html += '</div>';
    html += '</div>';

    // If this is general, put some space between these tags and the first category tag.
    if (category == "[GENERAL]") {
      html += '<div class="row  py-1">';
      html += '<div class="col tag-cell-text tag-cell-content-text">';
      html += '</div>';
      html += '</div>';
    }
  }  

  return html;
}

// Update the list of visible repository cards when a tag checkbox is checked / unchecked.
function UpdateRepositoryFilter(ButtonId) {
  // Get the ID of the tag that was just selected / unselected.
  var tagId = ButtonId.replace('btncheck', '');
  
  // Add or remove the current checkbox from the list of selected checkboxes.
  if ($('#' + ButtonId).is(':checked')) {
    if (jQuery.inArray(tagId, SelectedTags) == -1) {
      $('.repository-card').attr("hidden", true);
      SelectedTags[SelectedTags.length] = tagId;
    }
  } else {
	$('.repository-card').attr("hidden", false);
    SelectedTags.splice( $.inArray(tagId, SelectedTags), 1 );
  }

  FilterRepositories();
}

// Upodate the list of displayed repositories based on search text and selected filters.
function FilterRepositories() {
  var SearchText = $("#repository-search").val().toLowerCase().trim();
  // Clear previous filters.
  $('.repository-card').attr("hidden", false);

  // Apply new filters
  if (SelectedTags.length > 0 || SearchText != '') {
    $.each($('input.card-assigned-tags'), function( key, value ) {
      var DisplayCard = true;
      if (SelectedTags.length > 0) {
        $.each(SelectedTags, function( key, tag ) {
          if (!~value.value.indexOf('|' + tag + '|')) {
            DisplayCard = false;
          }
        });
      }

      if (DisplayCard == true && SearchText != '') {
        if ((!~$('#' + value.id).parent().find('.card-title').text().toLowerCase().indexOf(SearchText)) && (!~$('#' + value.id).parent().find('.card-description').text().toLowerCase().indexOf(SearchText))) {
          DisplayCard = false;
        }
      }

      if (DisplayCard == false) {
        $('#' + value.id).parent().attr("hidden", true);
      }
    });
  }

  // If no results, display nothing found message, otherwise hide the message.
  if ($('.repository-card:visible').length == 0) {
    $('#container-repositories-empty-message').show();
  } else {
    $('#container-repositories-empty-message').hide();
  }
  
  // Update the URL.
  SetTagsPageWindowHash();
}

// Returns a unique string id that identifies each tag, or empty string if no match.
function GetTagId(TagName, TagCategory) {
  var returnValue = '';
  if (jQuery.inArray(TagCategory, TagIndexes) != -1) {
    returnValue = jQuery.inArray(TagCategory, TagIndexes);
  } else {
    return '';
  }
  returnValue += '_';
  if (jQuery.inArray(TagName, tags[TagCategory]) != -1) {
    returnValue += jQuery.inArray(TagName, tags[TagCategory]);
  } else {
    return '';
  }

  return returnValue;
}

// Converts a tag id back into it's original string value.
function GetFullTagName(TagId) {
  var TagIdPieces = TagId.split('_');
  if (TagIndexes[TagIdPieces[0]] == '[GENERAL]') {
    return tags[TagIndexes[TagIdPieces[0]]][TagIdPieces[1]];
  } else {
    return TagIndexes[TagIdPieces[0]] + ': ' + tags[TagIndexes[TagIdPieces[0]]][TagIdPieces[1]];
  }
}

// Update the URL to include the names of any selected tags.
function SetTagsPageWindowHash() {
  var WindowHash = 'Tags';
  if (SelectedTags.length > 0) {
    WindowHash += ':';
    var SelectedTagsString = '';
    $.each(SelectedTags, function( key, value ) {
      if (SelectedTagsString.length > 0) {
        SelectedTagsString += ',';
      }
      SelectedTagsString += GetFullTagName(value);
    });
    WindowHash += encodeURIComponent(SelectedTagsString);
  }
  window.location.hash = WindowHash;
}

// Get a list of up to 20 user / group projects (type) for the specified user / group (id).
function GetProjectsList(type, id, accessToken, pagenum) {
  // Build the request.
  var repositorySettings = {
    "async": true,
    "crossDomain": true,
    "url": apiSiteRoot + "/" + apiVersion + "/" + type + "/" + id + "/projects?per_page=20&page=" + pagenum,
    "method": "GET",
  }

  if (accessToken != '') {
    repositorySettings.headers = { "Authorization": 'Bearer ' + accessToken }
  }

  // Send the request
  $.ajax(repositorySettings).done(function (response, textStatus, xhr) {
    // Create a card for each project in the response.
    $.each( response, function( key, value ) {
      var CardHtml = BuildRepositoryCard(value);
      if (CardHtml != '') {
        $('#repositoryContainer').append(CardHtml);
        $('#container-repositories-empty-message').hide();
      }
    });

    // If there's more then one page, send another request for the next batch of projects.
    var nextPage = xhr.getResponseHeader('X-Next-Page');
    if (nextPage == '') {
      ajaxCalls--;
    } else {
      GetProjectsList(type, id, accessToken, nextPage)
    }
  });
}

// Load and display the readme for the selected project.
function DisplayDocumentation(ProjectId) {
  // Mask the page.
  $('#loading').show();

  // Reset the screen
  $('#container-doc').html('');
  $("#card-doc-title").text($('#cardNamespace' + ProjectId).val());
  $clamp($("#card-doc-title")[0], {clamp: 'auto'});
  $(document).prop('title', SiteName + ': ' + $('#cardNamespace' + ProjectId).val());
  $('#cardDocFooter').html($('#cardFooter' + ProjectId).html());
  $('#btn-view-repository').attr('href', $('#cardRepositoryUrl' + ProjectId).val());

  // Figure out where the readme file is located at.
  var readmeField = $('#cardReadme' + ProjectId).val();
  var readmeLocation = readmeField.substring(readmeField.indexOf('/blob/')+6)
  var readmeBranch = readmeLocation.substring(0, readmeLocation.indexOf('/'));
  var readmeLocation = readmeLocation.substring(readmeLocation.indexOf('/') + 1);

  // Build the request.
  var accessToken = SignIn();
  var docFileSettings = {
    "async": true,
    "crossDomain": true,
    "url": apiSiteRoot + "/" + apiVersion + '/projects/' + ProjectId + '/repository/files/' + readmeLocation + '/raw?ref=' + readmeBranch + '',
    "method": "GET",
  }

  if (accessToken != '') {
    docFileSettings.headers = { "Authorization": 'Bearer ' + accessToken }
  }

  // Send the request
  $.ajax(docFileSettings).done(function (response, textStatus, xhr) {
    var md = window.markdownit();
    var html = md.render(response);
    var $html = $('<div />',{html:html});

    // fix links
    var mainBranch = $('#cardDefaultBranch' + ProjectId).val();
    $html.find('a[href]').each(function(){ 
      var oldUrl = $(this).attr("href");
      var newUrl = oldUrl;
      if (oldUrl.startsWith('./')) {
        newUrl = $('#cardRepositoryUrl' + ProjectId).val() + '/-/raw/' + mainBranch + oldUrl.substring(1);
      } else if (oldUrl.startsWith('/')) {
        newUrl = $('#cardRepositoryUrl' + ProjectId).val() + oldUrl;
      }
      $(this).attr("href", newUrl);
      $(this).attr('target','_blank');
    });

    // fix image locations
    $html.find('img[src]').each(function(){ 
      var oldUrl = $(this).attr("src");
      var newUrl = oldUrl;
      if (oldUrl.startsWith('./')) {
        newUrl = $('#cardRepositoryUrl' + ProjectId).val() + '/-/raw/' + mainBranch + oldUrl.substring(1);
      } else if (oldUrl.startsWith('/')) {
        newUrl = $('#cardRepositoryUrl' + ProjectId).val() + oldUrl;
      }
      $(this).attr("src", newUrl);
    });

    // render
    $('#container-doc').html($html.html());
	
  // Unmask the page.
  $('#loading').hide();
  });
}

// Do any final clean-ups after all data has been loaded.
function PageLoadComplete() {
  // Truncate long card titles and descriptions.
  $('.card-title').each(function(index, element) {
    $clamp(element, {clamp: 'auto'});
  });
  $('.card-description').each(function(index, element) {
    $clamp(element, {clamp: 'auto'});
  });

  // Sort the list of cards.
  $('.body-cards .repository-card').sort(function(a,b) {
    return $(a).find(".card-title").text().toLowerCase() > $(b).find(".card-title").text().toLowerCase() ? 1 : -1;
  }).appendTo(".body-cards");

  // Display tag filter buttons.
  $('#TagsContainer').append(BuildTagsScreen());

  // If a starting page was specified, navigate to it.
  navigate($(location).attr('hash'));

  // Unmask the page.
  $('#loading').hide();
}

// Load data into the grid on the History tab.
function LoadHistoryGrid() {
  $.ajax({
    type: 'GET',
    url: 'data/history.xml',
    crossDomain: true,
    dataType: "xml",
    success: function(data){
      $(data).find('History').each(function() {
        var row = $(this);
        var project_id = row.find('Project_Id').text();
        if (~LoadedProjectIds.indexOf('|' + project_id + '|')) { // Only display history for projects that are loaded on the projects screen.
          var project_date = row.find('Release_Date').text();
          var project_name = row.find('Project_Name').text();
          var project_number = row.find('Version_Number').text();
          var html = '<tr><td>' + project_id + '</td><td>' + project_date + '</td><td>' + project_name + '</td><td>' + project_number + '</td></tr>';
          $('#grid-history').append($(html));
        }
      });

      var bootgrid =  $("#grid-history").bootgrid({
        rowSelect: true,
        navigation: 3,
        sorting: false,
        columnSelection: false,
        rowCount: 10,
        caseSensitive: false
      });

      $("#grid-history").on('click.rs.jquery.bootgrid', function (e, cols, row, target) {
        if (typeof row != "undefined") {
          $('#repository-card-link-' + row.Id).click();
        }
      });

      PageLoadComplete();
    }
  });
}

// Toggle selected button when Tags is clicked -- set to Repositories
$(function() {
  $("#Tags").on('hide.bs.collapse', function(e) {
    $('a[id="nav-link-tags"]').removeClass('text-secondary').addClass('header-link'); // Unselect the tags button
    $('a[id="nav-link-repositories"]').removeClass('header-link').addClass('text-secondary'); // Select the main button.
    $(document).prop('title', SiteName + ': Projects');
    window.location.hash = "Projects";
  })
});

// Toggle selected button when Tags is clicked -- set to Tags
$(function() {
  $("#Tags").on('show.bs.collapse', function(e) {
    $('main').attr("hidden", true); // Hide everything.
    $('a[id^="nav-link-"], a.text-secondary').removeClass('text-secondary').addClass('header-link'); // Unselect all buttons
    $('#container-repositories').attr("hidden", false); // Show the main screen.
    $('a[id="nav-link-tags"]').removeClass('header-link').addClass('text-secondary'); // Select the tags button.
    SetTagsPageWindowHash();
    $(document).prop('title', SiteName + ': Tags');
  })
});

$( document ).ajaxError(function() {
  var accessToken = SignIn();
  if (accessToken != '') {
    SignOut();
  } else {
    PageLoadComplete();
  }
});

// Initial setup on page load
jQuery(document).ready(function($){
  // Update list as search text is changed.
  $("#repository-search").on('input', function() {
    FilterRepositories();
  });

  // Set Page Title
  $(document).prop('title', SiteName + ': Projects');

  // Display tooltips
  var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
  var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl);
  });

  // Initially hide logout until we confirm if there's a valid session token.
  $('#li-link-logout').hide();

  // Initialize tag categories.
  TagIndexes[TagIndexes.length] = '[GENERAL]';
  TagIndexes[TagIndexes.length] = 'Started';
  TagIndexes[TagIndexes.length] = 'Updated';
  TagIndexes[TagIndexes.length] = 'Environment';

  // Check if user is logged in, and if so, load login data.
  ajaxCalls++;
  var accessToken = SignIn();
  if (accessToken != '') {
    var loginSettings = {
      "async": true,
      "crossDomain": true,
      "url": apiSiteRoot + "/" + apiVersion + "/user",
      "method": "GET",
    }
    loginSettings.headers = { "Authorization": 'Bearer ' + accessToken }

    ajaxCalls++;
    $.ajax(loginSettings).done(function (response) {
      // Login successful, update screen.
      $("#profileImage").attr("src", response.avatar_url);
      $("#profileName").text(response.username)
      $("#profileURL").attr("href", response.web_url);
      $('#li-link-logout').show();
      $('#li-link-login').hide();
      ajaxCalls--;
    });
  }
  
  $.each( UsersToLoad, function( key, value ) {
    ajaxCalls++;
    GetProjectsList(key, value, accessToken, 1);
  });
  ajaxCalls--;
});

$( document ).ajaxComplete(function() {
  if (ajaxCalls == 0) {
    ajaxCalls--; // Only hit this function once.
    LoadHistoryGrid(); // Need to have the projects loaded first before loading the project history.
  }
});
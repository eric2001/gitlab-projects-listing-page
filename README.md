# GitLab Projects Listing Page
![GitLab Projects Listing Page](./screenshot.png) 

## Description
Creates a GitLab page that displays a list of all projects for the specified user(s) or group(s).

**License:** [GPL v.3](http://www.gnu.org/copyleft/gpl.html)

## Instructions
Copy this project.

Set up a new GitLab application.
 - Go to the Preferences screen for your user on GitLab (https://gitlab.com/-/profile/applications) or under the Settings for your group (https://gitlab.com/groups/GROUPNAME/-/settings/applications) and select Applications.
 - Create a new Application.  Enter in something meaningful for the Name field.  Set the Redirect URL field to the location of the published SignIn.html page (ex: http://USERNAME.gitlab.io//SignIn.html).  Under Scopes check "read_user", "read_api" and "read_repository".  Do not check any other checkboxes.  Click Save Application.  Copy down the generated Application ID value, and enter it into the ApplicationId field in the build_site.sh file as described below.

The following settings can be customized by editing the build_site.sh file:
- **PageTitle:**  Enter in the name to display for the page title here.
- **PageTheme:**  Specify which CSS file to use, the default is "dark".  A "light" option is also included.
- **RedirectURL:**  Specify the URL for the SignIn.html page on your published GitLab page.
- **ApplicationId:**  Enter in the Application Id for this GitLab application.
- **FooterText:**  Enter in any text that you would like displayed at the bottom of the page.

- **ApiSiteRoot:**  The page URL for API calls.  This is defaulted to "https://gitlab.com/api" for GitLab hosted pages.
- **ApiVersion:**  The version of the GitLab API to use.  This is defaulted to "v4".
- **OAuthURL:**  The URL to make a call to for a access token.  This is defaulted to "https://gitlab.com/oauth/token" for GitLab hosted pages.
- **UsersToLoad:** List of GitLab users and groups to load a list of projects from.  Should be in the format of "type":"name".  Separate multiple users / groups with commas.  Example:    "users": 'MYUSERNAME', "groups": 'MYGROUPNAME'
- **ProjectsToSkip:** Comma separated list of any projects that should not be displayed.  Leave blank to display everything.  Namespace must be included in the name.  Example:  'username/project1', 'groupname/project2'
- **CreatorId:** If set, this will restrict the list of projects to be displayed to a specific user.  Default value is "null" (disabled).

- **CardTagDefaultCss:** Specifies any CSS to use to format the tags displayed at the bottom of each card.  Default value is bg-light text-dark.
- **CardTagDefaultCss:** Allows for category specific overrides of the CardTagDefaultCss value.  Example:  If you set this to '[GENERAL]' :  'bg-success text-light', 'Latest Release' : 'bg-warning text-dark' then tags without a category will use bg-success text-light and tags in the "Latest Release" category will use bg-warning text-dark instead of the CSS specified in CardTagDefaultCss.
- **TagCategorySortOrder:** This project will display a sorted list of any assigned tags at the bottom of each Project.  Anything with a ":" in it will be displayed last, and will be split out from rest of the tags into its own section on the search page.  If the left side of any tags that contain a ":" is found in this list, then it will be displayed first, before anything else.  Ex:  If you tag a project with "Latest Release: 1.0.0" and set TagCategorySortOrder to "Latest Release" then "Latest Release: 1.0.0" will show up on the tag lists first for each repository.

- **SearchDefaultTagCss:** Specifies any CSS to use to format the tags displayed in the search section.  Default value is btn-outline-light.
- **SearchTagSpecificCss:** Allows for category specific overrides of the SearchDefaultTagCss value.  Example:  If you set this to '[GENERAL]' : 'btn-outline-success' then tags without a category will use btn-outline-success instead of the CSS specified in SearchDefaultTagCss.
- **SearchCategoriesOrder:** By default this project will display any tags without a category in the search section first, then an alphabetical list of each category with it's corresponding tags after that section.  Any categories listed in this setting will be displayed at the begging of the tags section, before the rest of the alphabetical list.  Ex: 'Started', 'Updated'
- **SearchCategoriesHidden:** Any categories listed in this section will not be available in the search section.  Ex: 'Latest Release'

The History section of this page displays information from the data/history.xml file.  A sample xml file is included with this project.  You may also wish to see the [GitLab Projects Listing History Editor](https://gitlab.com/eric2001/gitlab-projects-listing-history-editor) project for a windows-based history.xml editor.

The history page can be easily hidden by adding the following CSS to the custom.css file, in the event that you do not want to use it:
> #nav-link-history {
>   display: none;
> }

Navigate to the projects Pages URL to see the published page.

Note:  If you forked this project for your own use, please go to your project's Settings and remove the forking relationship, which won't be necessary unless you want to contribute back to the upstream project.

## History
Version 1.5.0
> - Updated Bootstrap library to 5.3.2
> - Updated jQuery library to 3.7.1
> - Updated Markdown-it library to 14.0.0
> - Released on 23 December 2023.
>
> Download: [Version 1.5.0](/uploads/4d9740e995f1c4962d08def32ffaae53/gitlab-projects-listing-page150.zip)

Version 1.4.0
> - Bug Fix:  Updated Login button to work with GitLab SSO Changes.
> - Updated Markdown-it library to 13.0.1
> - Released on 22 May 2022.
>
> Download: [Version 1.4.0](/uploads/5bf2f04d9ab342ca3b382408b43b730a/gitlab-projects-listing-page140.zip)

Version 1.3.1
> - Bug Fix:  Fixed issue where filters don't always work correctly when more then 9 exist.
> - Released on 05 March 2022.
>
> Download: [Version 1.3.1](/uploads/f6e093b72b2c8cba13515192886c297d/gitlab-projects-listing-page131.zip)

Version 1.3.0
> - Updated Markdown-it library to 12.3.2
> - Updated Copyright notices to 2022
> - Released on 18 February 2022.
>
> Download: [Version 1.3.0](/uploads/c8191158fc962d8ca7989ecf778c1a1b/gitlab-projects-listing-page130.zip)

Version 1.2.0
> - Updated Bootstrap library to 5.1.0
> - Fixed bug where tags section sometimes exceeded maximum width on mobile devices causing horizontal scrolling.
> - Released on 06 November 2021.
>
> Download: [Version 1.2.0](/uploads/a4cbcbf1d3e5342c0b0d9c4885a853e2/gitlab-projects-listing-page120.zip)

Version 1.1.0
> - Fixed bug in build script.
> - Fixed issue where readme fails to load if main branch isn't named "master".
> - Updated CSS for logo on repository card to make the size more dynamic.
> - Updated Bootstrap library to 5.1.0
> - Updated Markdown-it library to 12.2.0
> - Released on 05 August 2021.
>
> Download: [Version 1.1.0](/uploads/131fcba8715b518263f885a7bf7ed93e/gitlab-projects-listing-page110.zip)

Version 1.0.0
> - Initial Release
> - Released on 20 July 2021.
>
> Download: [Version 1.0.0](/uploads/d4882f79132ec3eca56ba61546b9f1b6/gitlab-projects-listing-page100.zip)
